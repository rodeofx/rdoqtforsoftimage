import pysideuic
import shiboken
from PySide import QtGui, QtCore
import xml.etree.ElementTree as xml
from cStringIO import StringIO

import win32com.client
xsi = win32com.client.Dispatch('XSI.Application').Application

qt_dialogs_storage = []

def loadUiType(uiFile):
    """
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    http://nathanhorne.com/?p=451
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    f = open(uiFile, 'r')
    if f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        #Fetch the base_class and form class based on their type in the xml from designer
        form_class = frame['Ui_%s'%form_class]
        base_class = eval('QtGui.%s'%widget_class)
    return form_class, base_class


def wrapinstance(ptr, base=None):
    """
    Utility to convert a pointer to a Qt class instance (PySide/PyQt compatible)
    http://nathanhorne.com/?p=485

    :param ptr: Pointer to QObject in memory
    :type ptr: long or Swig instance
    :param base: (Optional) Base class to wrap with (Defaults to QObject, which should handle anything)
    :type base: QtGui.QWidget
    :return: QWidget or subclass instance
    :rtype: QtGui.QWidget
    """
    if ptr is None:
        return None
    ptr = long(ptr) #Ensure type
    if globals().has_key('shiboken'):
        if base is None:
            qObj = shiboken.wrapInstance(long(ptr), QtCore.QObject)
            metaObj = qObj.metaObject()
            cls = metaObj.className()
            superCls = metaObj.superClass().className()
            if hasattr(QtGui, cls):
                base = getattr(QtGui, cls)
            elif hasattr(QtGui, superCls):
                base = getattr(QtGui, superCls)
            else:
                base = QtGui.QWidget
        return shiboken.wrapInstance(long(ptr), base)
    elif globals().has_key('sip'):
        base = QtCore.QObject
        return sip.wrapinstance(long(ptr), base)
    else:
        return None

def getQtSoftimageAnchor():
    # make sure the QApplication is launched
    sianchor = xsi.rdoGetQtSoftimageAnchor()

    # this should work but attaching a widget to the returned anchor crashses Soft ...
    # sianchor = Qt.wrapinstance( long(sianchor), QtGui.QWidget )

    # ... so return the top level window which should be the same as our anchor
    sianchor = QtGui.QApplication.topLevelWidgets()[0]

    return sianchor

def store_qt_dialog(dialog):
    global qt_dialogs_storage
    qt_dialogs_storage.append(dialog)
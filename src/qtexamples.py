import sys, os
if not "/mnt/users/julien/sources/rdoQtForSoftimage/src" in sys.path:
    sys.path.insert(0,"/mnt/users/julien/sources/rdoQtForSoftimage/src")
    sys.path.insert(0,"/mnt/users/julien/sources/rdoQtForSoftimage/src/Qt")
import Qt
#import win32com.client
from PySide import QtCore, QtGui, QtUiTools

class ExampleDialog( QtGui.QDialog ):
    def __init__( self, parent ):
        QtGui.QDialog.__init__( self, parent )

        self.setGeometry( 100, 100, 200, 100 )
        self.setWindowTitle( "Hello World" )
        self.setToolTip( "This is a <b>QWidget</b> widget" )

        self.btn = QtGui.QPushButton( "Log Text", self )
        self.btn.setToolTip( "This is a <b>QPushButton</b> widget" )
        self.btn.resize( self.btn.sizeHint() )
        self.btn.clicked.connect( self.logText )

        self.lineedit = QtGui.QLineEdit( "Hello World", self )
        self.lineedit.setToolTip( "Type Something" )

        layout = QtGui.QVBoxLayout( self )
        layout.addWidget( self.lineedit )
        layout.addWidget( self.btn )

    def logText( self ):
        Application.LogMessage( self.lineedit.text() )

class ExampleSignalSlot( ExampleDialog ):
    def __init__( self, parent ):
        ExampleDialog.__init__( self, parent )
        self.setWindowTitle( "Signal/Slot Example" )
        self.lineedit.setText( "" )

        # module containing sievents mapped to pyqtsignals
        from sisignals import signals, muteSIEvent

        # connect the siActivate signal to the activate slot
        signals.siActivate.connect( self.activate )
        muteSIEvent( "siActivate", False )

        # connect the siPassChange signal to the passChanged slot
        signals.siPassChange.connect( self.passChanged )
        muteSIEvent( "siPassChange", False )

    def activate( self, state = None ):
        if state is not None:
            if state:
                self.lineedit.setText( "Welcome Back!" )
            else:
                self.lineedit.setText( "Good Bye!")

    def passChanged( self, targetPass = "" ):
        self.lineedit.setText( targetPass )

    def closeEvent( self, event ):
        # disconnect signals from slots when you close the widget
        # muteSIEvent() can be used to mute the signals softimage events send
        # but be careful if another widget exists and is using them
        from sisignals import signals, muteSIEvent
        signals.siActivate.disconnect( self.activate )
        signals.siPassChange.disconnect( self.passChanged )
        #muteSIEvent( "siActivate", True )
        #muteSIEvent( "siPassChange", True )

class ExampleMenu( QtGui.QMenu ):
    def __init__( self, parent ):
        QtGui.QMenu.__init__( self, parent )

        # add actions and a separator
        hello = self.addAction("Print 'Hello!'")
        self.addSeparator()
        world = self.addAction("Print 'World!'")

        # connect to the individual action's signal
        hello.triggered.connect( self.hello )
        world.triggered.connect( self.world )

        # connect to the menu level signal
        self.triggered.connect( self.menuTrigger )

    def hello( self ):
        Application.LogMessage( "Hello!" )

    def world( self ):
        Application.LogMessage( "World!" )

    def menuTrigger( self, action ):
        if action.text() == "Print 'Hello!'":
            Application.LogMessage( "You clicked, Print 'Hello!'" )
        elif action.text() == "Print 'World!'":
            Application.LogMessage( "You clicked, Print 'World!'" )

# call loadUiType to compile the ui file on the fly
uifilepath = os.path.join(os.path.dirname(__file__),"exampleui.ui")
dialog_form, dialog_base = Qt.loadUiType(uifilepath)
# use the object
class ExampleUIFile( dialog_form, dialog_base ):
    def __init__( self, parent):
        super(ExampleUIFile,self).__init__(parent)
        self.setupUi(self)

        # connect to the createCube function
        try:
            self.uiCreateCube.clicked.connect( self.createCube )
        except Exception, e:
            Application.LogMessage(e)

    def createCube( self ):
        try:
            cube = Application.CreatePrim("Cube", "MeshSurface", self.uiCubeName.text(), "")
            cube.Length.Value = self.uiCubeLength.value()
        except Exception, e:
            Application.LogMessage(e)


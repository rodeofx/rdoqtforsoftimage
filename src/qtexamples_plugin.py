import sys
import win32com.client
from PySide import QtGui, QtCore
import qtexamples

PLUGIN_PATH = __sipath__
if PLUGIN_PATH not in sys.path:
    sys.path.append(PLUGIN_PATH)
import Qt

def XSILoadPlugin( in_reg ):
    in_reg.Name = "PyQt_Example"
    in_reg.Author = "Steven Caron"
    in_reg.RegisterCommand( "ExampleDialog" )
    in_reg.RegisterCommand( "ExampleSignalSlot" )
    in_reg.RegisterCommand( "ExampleMenu" )
    in_reg.RegisterCommand( "ExampleUIFile" )

def ExampleDialog_Execute():
    """a simple example dialog showing basic functionality of the pyqt for softimage plugin"""
    sianchor = Qt.getQtSoftimageAnchor()
    dialog = qtexamples.ExampleDialog( sianchor )
    dialog.show()

def ExampleSignalSlot_Execute():
    """a simple example showing softimage events triggering pyqt signals"""
    sianchor = Qt.getQtSoftimageAnchor()
    dialog = qtexamples.ExampleSignalSlot( sianchor )
    dialog.show()

def ExampleMenu_Execute():
    """a simple example showing the use of a qmenu"""
    sianchor = Qt.getQtSoftimageAnchor()
    menu = qtexamples.ExampleMenu( sianchor )

    # notice the use of QCursor and exec_ call
    menu.exec_(QtGui.QCursor.pos())

def ExampleUIFile_Execute():
    """a simple example showing the use of a .ui file created using QtDesigner"""
    # PySide way of loading ui files is a real pain compared to PyQt.
    # I've found three different ways of doing it:
    #   - compile your ui file with pysideuic and import the resulting python file
    #   - compile the ui file on the fly
    #   - use the QtUiTools.QUiLoader class to create a widget that you add to your current widget
    #
    # I use the second one because I like the flexibility of not having to compile the UI file each time I modify it
    # and because the last one doesn't seem to work properly in the context of Softimage

    sianchor = Qt.getQtSoftimageAnchor()
    dialog = qtexamples.ExampleUIFile(None)
    dialog.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

    # PySide garbage collector is very aggressive, and as said in PySide pitfalls (http://qt-project.org/wiki/PySide_Pitfalls)
    #
    # "If a QObject falls out of scope in Python, it will get deleted. You have to take care of keeping a reference to the object:
    #   - Store it as an attribute of an object you keep around, e.g. self.window = QMainWindow()
    #   - Pass a parent QObject to the object’s constructor, so it gets owned by the parent"
    #
    # When we compile the ui file on the fly in the qtexamples file, the QObject lives here and looks like it goes out of scope as soon as we leave
    # the execution of the command. So I stuff the dialog somewhere where it could stay.
    Qt.store_qt_dialog(dialog)

    dialog.show()
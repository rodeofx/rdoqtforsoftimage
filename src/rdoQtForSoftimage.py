import sys
import win32com.client
from win32com.client import constants
xsi = win32com.client.Dispatch('XSI.Application').Application

import shiboken
from PySide import QtCore, QtGui

g_rdoSIAnchor = None


def XSILoadPlugin( in_reg ):
    in_reg.Author = "Julien Dubuisson"
    in_reg.Name = "rdoQtForSoftimage"
    in_reg.Major = 1
    in_reg.Minor = 0

    in_reg.RegisterCommand("rdoGetQtSoftimageAnchor","rdoGetQtSoftimageAnchor")
    in_reg.RegisterCommand("rdoCloseQtSoftimageAnchor","rdoCloseQtSoftimageAnchor")
    in_reg.RegisterCommand("rdoKillQtSoftimageAnchor","rdoKillQtSoftimageAnchor")
    in_reg.RegisterTimerEvent( "rdoQtTimer", 20, 0 );

    return True

def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    xsi.LogMessage(str(strPluginName) + str(" has been unloaded."),constants.siVerbose)

    if g_rdoSIAnchor:
        g_rdoSIAnchor.close()

    return True

def rdoGetQtSoftimageAnchor_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.ReturnValue = True

    return True

def rdoGetQtSoftimageAnchor_Execute(  ):
    global g_rdoSIAnchor
    xsi.LogMessage("rdoGetQtSoftimageAnchor_Execute called",constants.siVerbose)
    if not QtCore.QCoreApplication.instance():
        QtGui.QApplication(sys.argv)

    xsi.EventInfos( "rdoQtTimer" ).Mute = False
    if not g_rdoSIAnchor:
        g_rdoSIAnchor = QtGui.QWidget()
        g_rdoSIAnchor.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        g_rdoSIAnchor.setWindowTitle("QtSoftimageAnchor")

    g_rdoSIAnchor.resize(250,1)
    g_rdoSIAnchor.show()
    return id(g_rdoSIAnchor)

def rdoKillQtSoftimageAnchor_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.ReturnValue = True

    return True

def rdoKillQtSoftimageAnchor_Execute(  ):
    global g_rdoSIAnchor
    xsi.LogMessage("rdoGetQtSoftimageAnchor_Execute called",constants.siVerbose)
    if g_rdoSIAnchor:
        g_rdoSIAnchor.close()
        del g_rdoSIAnchor
        g_rdoSIAnchor = None

    if QtCore.QCoreApplication.instance():
        QtCore.QCoreApplication.instance().exit()
        xsi.EventInfos( "rdoQtTimer" ).Mute = True
    return True

def rdoCloseQtSoftimageAnchor_Init( in_ctxt ):
    oCmd = in_ctxt.Source
    oCmd.Description = ""
    oCmd.ReturnValue = True

    return True

def rdoCloseQtSoftimageAnchor_Execute(  ):
    global g_rdoSIAnchor
    xsi.LogMessage("rdoCloseQtSoftimageAnchor_Execute called",constants.siVerbose)
    g_rdoSIAnchor.close()

    return True

def rdoQtTimer_OnEvent( in_ctxt ):
    if QtCore.QCoreApplication.instance():
        QtCore.QCoreApplication.processEvents()